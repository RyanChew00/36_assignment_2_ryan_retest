﻿using System;

namespace BowlingGame_Retest
{
    public class Game
    {

        int[] pinFalls = new int[21];
        int rollCounter;

        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public void Roll(int pins)
        {
            pinFalls[rollCounter] = pins;
            rollCounter++;
        }

        public int Score()
        {
            int score = 0;
            int i = 0;
            for (int frame = 0; frame < 10;frame++)
            {
                if (pinFalls[i]+pinFalls[i+2] == 20)
                {
                    score += 20 + pinFalls[i + 2];
                    i += 1;
                }

                else if(pinFalls[i]+pinFalls[i+1]==10)
                {
                    score += 10 + pinFalls[i + 2];
                    i += 2;
                }
                else
                {
                    score += pinFalls[i] + pinFalls[i + 1];
                    i += 2;
                }
            }
            return score;
        }
    }
}
